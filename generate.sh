#!/usr/bin/env sh

echo '
stages:
  - dynamic' >> dynamic.yml

tpl='
execute JOBNAME:
  stage: dynamic
  image: alpine:latest
  script:
    - sh file.sh FILENAME'

reports=($DEPENDENCY_SCANNING_REPORT )
num=0
for r in ${reports[@]}; do 
  if [ -f $r ]; then 
    echo "generating job for $r"
    echo "$tpl" | sed "s#FILENAME#$r#g" | sed "s#JOBNAME#$r#g" >> dynamic.yml
    num=$((num+1))
  fi
done

if [ "$num" -eq 0 ]; then
  echo '
dummy job:
  stage: dynamic
  script:
    - echo "this job is here in case no reports were found"' >> dynamic.yml
fi
